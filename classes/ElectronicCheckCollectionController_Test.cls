@isTest
public class ElectronicCheckCollectionController_Test 
{
	@isTest static void TestGetECCSubmissionDeadlinePassed1()
    {
        
        Test.startTest();
        Datetime dt = Datetime.newinstance(2019, 3,11,10,0,0);		// year, month, day, hour, min, sec
        Boolean bok = ElectronicCheckCollectionController.GetECCSubmissionDeadlinePassed(dt);        
        Test.stopTest();
        
    }
    
    	@isTest static void TestGetECCSubmissionDeadlinePassed2()
    {
        
        Test.startTest();
        Datetime dt = Datetime.newinstance(2019, 3,12,10,0,0);		// year, month, day, hour, min, sec
        Boolean bok = ElectronicCheckCollectionController.GetECCSubmissionDeadlinePassed(dt);        
        Test.stopTest();
        
    }
	@isTest static void TestGetECCSubmissionDeadlinePassed3()
    {
        
        Test.startTest();
        Datetime dt = Datetime.newinstance(2019, 3,13,10,0,0);		// year, month, day, hour, min, sec
        Boolean bok = ElectronicCheckCollectionController.GetECCSubmissionDeadlinePassed(dt);        
        Test.stopTest();
        
    }
	@isTest static void TestGetECCSubmissionDeadlinePassed4()
    {
        
        Test.startTest();
        Datetime dt = Datetime.newinstance(2019, 3,14,10,0,0);		// year, month, day, hour, min, sec
        Boolean bok = ElectronicCheckCollectionController.GetECCSubmissionDeadlinePassed(dt);        
        Test.stopTest();
        
    }
	@isTest static void TestGetECCSubmissionDeadlinePassed5()
    {
        
        Test.startTest();
        Datetime dt = Datetime.newinstance(2019, 3,15,10,0,0);		// year, month, day, hour, min, sec
        Boolean bok = ElectronicCheckCollectionController.GetECCSubmissionDeadlinePassed(dt);        
        Test.stopTest();
        
    }
	@isTest static void TestGetECCSubmissionDeadlinePassed6()
    {
        
        Test.startTest();
        Datetime dt = Datetime.newinstance(2019, 3,16,10,0,0);		// year, month, day, hour, min, sec
        Boolean bok = ElectronicCheckCollectionController.GetECCSubmissionDeadlinePassed(dt);        
        Test.stopTest();
        
    }
	@isTest static void TestGetECCSubmissionDeadlinePassed7()
    {
        
        Test.startTest();
        Datetime dt = Datetime.newinstance(2019, 3,17,10,0,0);		// year, month, day, hour, min, sec
        Boolean bok = ElectronicCheckCollectionController.GetECCSubmissionDeadlinePassed(dt);        
        Test.stopTest();
        
    }

    @isTest static void TestfetchContact()
    {
                Account objAccount = new Account();
        objAccount.name = 'some';
        insert objAccount;

      	//Contact objcontact = [Select Id, AccountId	 From Contact where lastname = 'Admin'];
      	//List<Contact> objContactList = [Select Id, AccountId From Contact];
              	Contact objContact = new Contact();
        objContact.LastName = 'xxx';
        objContact.AccountId = objAccount.Id;
        insert objContact;
        Id objContactId = objContact.Id;
        Contact obj2 = ElectronicCheckCollectionController.fetchContact(objContactId);  
    }
    @isTest static void TestfetchContactAccount()
        {
                    Account objAccount = new Account();
        objAccount.name = 'some';
        insert objAccount;

      	//Contact objcontact = [Select Id, AccountId	 From Contact where lastname = 'Admin'];
      	//List<Contact> objContactList = [Select Id, AccountId From Contact];
              	Contact objContact = new Contact();
        objContact.LastName = 'xxx';
        objContact.AccountId = objAccount.Id;
                    insert objContact;

        Id objContactId = objContact.Id;
            Account objacc = ElectronicCheckCollectionController.fetchContactAccount(objContactId);  
        }
   @isTest static void TestfetchUser()
        {
            user obj = ElectronicCheckCollectionController.fetchUser();
        }

     @isTest static void TestfetchUserAccount()
     {
         
         Account obj = ElectronicCheckCollectionController.fetchUserAccount();
     }
         
         
    @isTest static void TestSaveBatch()
    {
     	         Account objAccount = new Account();
        objAccount.name = 'some';
        insert objAccount; 
          
      	//Contact objcontact = [Select Id, AccountId	 From Contact where lastname = 'Admin'];
      	//List<Contact> objContactList = [Select Id, AccountId From Contact];
              	Contact objContact = new Contact();
        objContact.LastName = 'xxx';
        objContact.AccountId = objAccount.Id;
                objContact.Email = 'mmclean@synergentcorp.com';

                insert objContact;

        Id objContactId = objContact.Id;
		
        //Account objAccount = new Account();
        //objAccount.name = 'some';
        
        Case objNewCase = new Case();
        objNewCase.Subject = 'ECC Form Submission';
        objNewCase.Description = 'Form submitted thru community portal.';
        objNewCase.Service_Area__c = 'Check Processing';
        objNewCase.Category__c = 'Adjustment';
        objNewCase.Sub_Category__c = 'Other';
        
        objNewCase.Status = 'New';
        objNewCase.Type = 'Customer Service Request';
        // maybe not 
        objNewCase.Origin = 'Synergent Case Portal';
        objNewCase.Product_Type__c = 'Check_Processing';
        objNewCase.AccountId = objAccount.Id;
                 objNewCase.ContactId = objContactId;      
         
        Id idRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('External Service Request').getRecordTypeId();
        objNewCase.RecordTypeId = idRecordTypeId;
        insert objNewCase;
		ID objCaseId = objNewCase.Id;
        
        Electronic_Check_Collection_Batch__c obj = new Electronic_Check_Collection_Batch__c();
        
        obj.Create_Date__c = System.Date.today();
        obj.Person_Requesting_Action__c = objContactId;
        obj.Action_Requested__c = 	'Item Delete';
        obj.Case__c = objCaseId;
        obj.AccountId__c = objContact.AccountId;
        obj.Name = 'ECC Submission Batch';
        obj.Batch_Dollar_Amount__c = 9;
        obj.Batch_Item_Count__c = 1;
        obj.Batch_Number__c = 'ccc';
        obj.Date_Scanned__c = System.date.today();
        obj.Name ='some';
        obj.Reason_For_Batch_Action__c = 'Other';
        

           List<Electronic_Check_Collection_Batch_Item__c> objItemList = new List<Electronic_Check_Collection_Batch_Item__c>();
        Electronic_Check_Collection_Batch_Item__c objItem = new Electronic_Check_Collection_Batch_Item__c();
        objItem.Name = 'some';
        objItem.Item_Action__c = 'Delete';
        //objItem.
        
        objItemList.Add(objItem);
        
        Electronic_Check_Collection_Batch__c objzzzz = ElectronicCheckCollectionController.SaveBatch(obj, objItemList,objCaseId,objContactId);
        
        
               Electronic_Check_Collection_Batch__c obj1 = ElectronicCheckCollectionController.GetBatch(objzzzz.Id);

        
    }
}