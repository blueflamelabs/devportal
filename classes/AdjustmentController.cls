public with sharing class AdjustmentController 
{
    @AuraEnabled
    public static Adjustment__c Save(Adjustment__c obj, ID objCaseId,ID objContactId) 
    {
        ID iContactId = objContactId;
        ID iCaseId = objCaseId;
        
        system.debug('In Adjustment Save********************************************************************************************************' );

        //
        obj.Case__c = iCaseId;
        
        obj.Name = 'Adjustment Submission';
        
        insert obj;
        
         system.debug('Inserted  Adjustment********************************************************************************************************' );
        
        system.debug('Exiting Adjustment Save********************************************************************************************************' );
        
        return obj;

    }
            @AuraEnabled 
    public static Adjustment__c GetAdjustment(Id recordId)
    {
     	// query  
      	Adjustment__c obj = [select id,Name	 FROM Adjustment__c Where id =: recordId];
        return obj;
    }


}