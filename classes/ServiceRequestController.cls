public with sharing class ServiceRequestController 
{
    

    
    @AuraEnabled
    public static ID SaveECCCase( string strSubject,
                                              string strDescription,
                                              string strServiceArea,
                                              string strCategory,
                                              string strSubCategory,
                                              string strPriority,
                                 				Electronic_Check_Collection_Batch__c obj,
                                  List<Electronic_Check_Collection_Batch_Item__c> objItemList,
                                 				ID objContactId)
    {
        ID iReturnId;
        // Check subvmission deadlines here so we do not create a case before exception
        // 
        
        Boolean bSubmissionPassed = ElectronicCheckCollectionController.GetECCSubmissionDeadlinePassed();
        if (bSubmissionPassed)
        {
         	if(Test.isRunningTest())
            {}
            else
            {
            	///CalloutException e = new CalloutException();
            	//e.setMessage('Items can only be deleted/corrected the same day they are received by Synergent');
            	//throw e;
            	throw new AuraHandledException('Submission deadline has passed. See form for acceptable times, or contact Synergent.');   

            }
        }
        else
        {
        
			iReturnId = SaveServiceRequestCase(  strSubject,
                                               strDescription,
                                               strServiceArea,
                                               strCategory,
                                               strSubCategory,
                                               strPriority,objContactId );
        
       		// List<Electronic_Check_Collection_Batch_Item__c> objItemList = new List<Electronic_Check_Collection_Batch_Item__c>();
       		// 
  			system.debug('Case Id = ' + iReturnId + '******************************************************************************************************************');

        	SaveECC( obj, objItemList,iReturnId, objContactId);
        }
        
        return(iReturnId);
        
        
    }
    @AuraEnabled
    public static ID SaveDefaultCase( string strSubject,
                                              string strDescription,
                                              string strServiceArea,
                                              string strCategory,
                                              string strSubCategory,
                                              string strPriority,ID objContactId ) 
    {
    		ID iReturnId = SaveServiceRequestCase(  strSubject,
                                               strDescription,
                                               strServiceArea,
                                               strCategory,
                                               strSubCategory,
                                               strPriority,objContactId );
            return(iReturnId);

    }
    
    @AuraEnabled
    public static ID SaveServiceRequestCase( string strSubject,
                                              string strDescription,
                                              string strServiceArea,
                                              string strCategory,
                                              string strSubCategory,
                                              string strPriority,ID objContactId ) 
    {
        // 
        //
        string strAccountName = '';
        ID iAccountId;
        ID iContactId;
        
         system.debug(logginglevel.DEBUG, 'in SaveServiceRequestCase');
        if (objContactId != null)
        {
         	iContactId = objContactId;
            
      		Contact objContact = [select id,AccountId,Email,Name, Phone FROM Contact Where id =: objContactId];
 
     		Id accountid = '001L0000014m8IUIAY'; 	// temp hack
        	if (objContact.AccountId != null)
        	{
         		accountid = objContact.AccountId;   
        	}
        	else
        	{
                  
        	}
            
      		Account objAccount = [select id,Name FROM Account Where id =: accountid];
        	iAccountId = objAccount.Id;
			strAccountName = objAccount.Name;
        }
        //else
        //{        
            // User objUser = [select id,Name,Username,Email,FirstName,LastName, ContactId, AccountId FROM User Where id =: userInfo.getUserId()];
           // if ((objUser.ContactId != null) && (objUser.AccountId != null))
           // {        
            //      system.debug('Good contact data******************************************************************************************************************');
       
                   // Account objAccount = [select Name from Account Where id =: objUser.AccountId]; 
                   // strAccountName = objAccount.Name;
                   // iAccountId = objUser.AccountId;
                   // iContactId = objUser.ContactId; 
    
            //}
            //else
            //{
                 //system.debug('Bad Contact data********************************************************************************************************' );
       
                // assume user is internal and not in contact table
                   // string strRequestLastName = 'Community';
                    //string strRequestFirstName = '';
                   // ID iRequestAccountId;
                    //List<Contact> objContactList = [select id, AccountId from Contact Where LastName =: strRequestLastName AND FirstName =: strRequestFirstName];
                   // List<Contact> objContactList = [select id, AccountId, LastName, FirstName from Contact Where LastName =: strRequestLastName];
                   // for (Contact objContact : objContactList)
                   // {
                    //    if (objContact.AccountId != null)
                    //    {
                    //        iRequestAccountId = objContact.AccountId;
                            //Contact objContactHack = [select id, AccountId from Contact Where Email =: objUser.Email];
                            //obj.AccountId__c = iRequestAccountId;  
                    //        iContactId = objContact.Id;
                    ///        Account objAccount = [select Name from Account Where id =: iRequestAccountId];
                   //         if (objAccount != null)
                   //         {
                   //             strAccountName = objAccount.Name;
                   //             iAccountId = iRequestAccountId;
                   //         }   
                    //    }            
                   // }            
            //}               
        //}

        
        // Create the case object here
        Case objNewCase = new Case();
        objNewCase.Subject = strSubject;
        objNewCase.Description = strDescription;
        objNewCase.Service_Area__c = strServiceArea;
        objNewCase.Category__c = strCategory;
        
        //system.debug('debug BMM strSubCategory -' + strSubCategory);
        
        
        // bug here , need to be able to select none
         //if (String.IsNotEmpty(strSubCategory))
        // {
         	////String.IsNotBlank(strSubCategory))
        //	objNewCase.Sub_Category__c = strSubCategory;
        //}
        //
        if (strSubCategory != '--- None ---')
        {
        	objNewCase.Sub_Category__c = strSubCategory;
            
        }
        
        objNewCase.Priority = strPriority;

       // 
        //objNewCase.Subject =   strAccountName + ' ' + 'ECC Form Submission';
        //objNewCase.Description = 'Form submitted thru community portal.';
        objNewCase.Status = 'New';
        objNewCase.Type = 'Customer Service Request';
        // maybe not 
        objNewCase.Origin = 'Synergent Case Portal';
        objNewCase.Product_Type__c = 'Check_Processing';
        objNewCase.AccountId = iAccountId;
        objNewCase.ContactId = iContactId;
                
        Id idRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('External Service Request').getRecordTypeId();
        objNewCase.RecordTypeId = idRecordTypeId;
        
        try
        {
        	insert objNewCase;
        }
        catch (Exception e)
        {
            
            system.debug('error insert case' + e.getMessage());
        }
        
    
          system.debug('New Case created********************************************************************************************************' );

                
        return objNewCase.Id;

    }

  
    
    
    @AuraEnabled
    public static void SaveECC(Electronic_Check_Collection_Batch__c obj, 
                                                                 List<Electronic_Check_Collection_Batch_Item__c> objItemList,
                                                                 ID objCaseId,ID objContactId)
    {
     	          system.debug('In SaveECC********************************************************************************************************' );

        
        // Wrapper for Electronic Check Collection 
     	 Electronic_Check_Collection_Batch__c objBatchReturn = ElectronicCheckCollectionController.SaveBatch(obj, 
                                                                 objItemList,
                                                                  objCaseId,objContactId ); 
        
                List<Billing_Item__c> objBillingItemList = [Select Id, Name From Billing_Item__c where Name = 'Batch Deletions'];
        
        Id iBilling = SaveDefaultBilling(objCaseId, objBillingItemList[0] );
        objBatchReturn.Billing__c = iBilling;
        update objBatchReturn;

            	   system.debug('Exiting SaveECC********************************************************************************************************' );
 
    }
    @AuraEnabled
    public static ID SaveAdjustmentCase(string strSubject,
                                              string strDescription,
                                              string strServiceArea,
                                              string strCategory,
                                              string strSubCategory,
                                              string strPriority,
                                 				Adjustment__c obj,
                                 				ID objContactId)

    {
 ID iReturnId;
        
		iReturnId = SaveServiceRequestCase(  strSubject,
                                               strDescription,
                                               strServiceArea,
                                               strCategory,
                                               strSubCategory,
                                               strPriority,objContactId );
        
  	system.debug('Case Id = ' + iReturnId + '******************************************************************************************************************');

        SaveAdjustment( obj, iReturnId, objContactId);
        
        return(iReturnId);
        
        
       
    }
    @AuraEnabled
    public static Id SaveManualCase(string strSubject,
                                              string strDescription,
                                              string strServiceArea,
                                              string strCategory,
                                              string strSubCategory,
                                              string strPriority,
                                 				Manual__c obj,
                                 				ID objContactId)

    {
		ID iReturnId;
        //try
        //{
        iReturnId = SaveServiceRequestCase(  strSubject,
                                               strDescription,
                                               strServiceArea,
                                               strCategory,
                                               strSubCategory,
                                               strPriority,objContactId );
        //}
        //catch(Exception e)
        //{
        //    
        //}
  	system.debug('Case Id = ' + iReturnId + '******************************************************************************************************************');

        SaveManual( obj, iReturnId, objContactId);
        
        
        return(iReturnId);
   
    }

        @AuraEnabled
    public static void SaveAdjustment(Adjustment__c obj, ID objCaseId,ID objContactId)
    {
     	          system.debug('In SaveAdjustment********************************************************************************************************' );

        
        // Wrapper for Electronic Check Collection 
     	 Adjustment__c objReturn = AdjustmentController.Save(obj, objCaseId,objContactId ); 
            	   system.debug('Exiting SaveAdjustment********************************************************************************************************' );
         List<Billing_Item__c> objBillingItemList = [Select Id, Name From Billing_Item__c where Name = 'Adjustment'];
        
        Id iBilling = SaveDefaultBilling(objCaseId, objBillingItemList[0] );
        objReturn.Billing__c = iBilling;
        update objReturn;

    }
    @AuraEnabled
    public static void SaveManual(Manual__c obj,  ID objCaseId,ID objContactId)
    {
     	          system.debug('In SaveManual********************************************************************************************************' );

        
        // Wrapper for Electronic Check Collection 
     	 Manual__c objReturn = ManualController.Save(obj, objCaseId,objContactId ); 
            	   system.debug('Exiting SaveManual********************************************************************************************************' );
 
        List<Billing_Item__c> objBillingItemList = [Select Id, Name From Billing_Item__c where Name = 'Manual Handling'];
        
        Id iBilling = SaveDefaultBilling(objCaseId, objBillingItemList[0] );
        objReturn.Billing__c = iBilling;
        update objReturn;
        
    }
    @AuraEnabled
    public static Id SaveDefaultBilling(Id objCaseId, Billing_Item__c objBillingItem )
    {

        
        
        Date dtBillingEndDate = System.Date.today();
        Case objCase = [select Id, AccountId, CaseNumber From Case where Id =: objCaseId];
        Account objAccount = [Select Id, Name From Account where Id =: objCase.AccountId ];
        
        string strName = objAccount.Name + ' Case:' + objCase.CaseNumber + ' Billing';
            // Create object here
            // 
            
        Billing__c obj = new Billing__c();
        obj.Account__c = objAccount.Id;
        obj.Case__c = objCaseId;
        obj.Billing_Item__c = objBillingItem.Id;
        obj.Name = strName;
        obj.Period_End_Date__c = dtBillingEndDate;
        obj.Quantity__c = 1;
        
        try
        {
        	insert obj;
        }
        catch (Exception e)
        {
            
            system.debug('error insert case' + e.getMessage());
        }
        
    
          system.debug('New Billing created********************************************************************************************************' );

                
        return obj.Id;
    }
    
    //bmm add
    
           @AuraEnabled 
    public static user fetchUser()
    {
     // query current user information  
      User oUser = [select id,Name,TimeZoneSidKey,Username,Alias,Country,Email,FirstName,LastName,IsActive,IsPortalEnabled, ContactId 
                 FROM User Where id =: userInfo.getUserId()];
        return oUser;
    }
 
    
    @AuraEnabled 
    public static Account fetchUserAccount()
    {
     	// query current user information
      	User oUser = [select id,Name,TimeZoneSidKey,Username,Alias,Country,Email,FirstName,LastName,IsActive,IsPortalEnabled, ContactId, AccountId 
                 FROM User Where id =: userInfo.getUserId()];
 
     	Id accountid = '001L0000014m8IUIAY'; 	// temp hack
        if (oUser.AccountId != null)
        {
         	accountid = oUser.AccountId;   
        }
        else
        {
            // Get a list in case using more than one of the same emails as a temp hack
            List<Contact> objContactLst = [select id,Name,Email, AccountId 
                     FROM Contact Where Email =: oUser.Email];
            for (Contact objContact : objContactLst)
            {
                           
                if (objContact.AccountId != null)
                {            
                       accountid = objContact.AccountId;
                }
            }
                  
        }
      	Account objAccount = [select id,Name FROM Account Where id =: accountid];
        
        return objAccount;
    }

    
    
    ////
    @AuraEnabled
    public static List<PicklistOption> getStates(String selectedCountry, String objectAPIName,
                                                String countryPicklistAPIName, String statePicklistAPIName)
    {
        List<PicklistOption> stateList = new List<PicklistOption>();
        if(selectedCountry == null || selectedCountry == '')
        {
            return stateList;
        }
        Map<String,List<String>> valueMap = DependentPicklistUtils.getFieldDependencies(
            objectAPIName,countryPicklistAPIName,statePicklistAPIName);
        if(valueMap != null)
        {
            // add a blank at top of list to force user to set focus and select
            stateList.add(new PicklistOption('',''));
            for(String state: valueMap.get(selectedCountry))
            {
                if(state != null || state != '')
                {
                stateList.add(new PicklistOption(state,state));
                }
            }
        }
        return stateList;
    }
  
     public class PicklistOption 
     {
      
        @AuraEnabled
        public String label { get; set; }
      
        @AuraEnabled
        public String value { get; set; }
      
        public PicklistOption( String label, String value ) {
            this.label = label;
            this.value = value;
        }
    }
    
    
}