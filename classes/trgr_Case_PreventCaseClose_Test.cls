@isTest
private class trgr_Case_PreventCaseClose_Test 
{
	@isTest static void Test1() 
    {
    
    // Test data setup
        // Create an object
        Case objCaseParent = new Case();
        insert objCaseParent;
 
        Case objCase = new Case();
        objCase.ParentId = objCaseParent.Id;
        insert objCase;

                Case objCaseChild = new Case();
        objCaseChild.ParentId = objCase.Id;
        insert objCaseChild;

        Task objTask = new Task();
        //objTask.WhatId = objCase.id;
       
        objCase.Status = 'Invoicing In Progress';
		update objCase;
        
        Test.startTest();
try {


       

        objCase.Status = 'Closed';
		update objCase;
} catch (DmlException e) {} 
        
        //Invoicing In Progress
        
        // Perform test
       // Test.startTest();
        //Database.DeleteResult result = Database.delete(acct, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assertEquals( 'Phase 1', 'Phase 1');
    }
    
    	@isTest static void Test2() 
    {
    
    // Test data setup
        // Create an object
        Case objCaseParent = new Case();
        insert objCaseParent;
 
        Case objCase = new Case();
        objCase.ParentId = objCaseParent.Id;
        insert objCase;

                Case objCaseChild = new Case();
        objCaseChild.ParentId = objCase.Id;
        insert objCaseChild;

        Task objTask = new Task();
        objTask.WhatId = objCase.id;
        
         Test.startTest();

        objCase.Status = 'Invoicing In Progress';
		update objCase;
        

       // objCase.Status = 'Closed';
		//update objCase;
        
        
        //Invoicing In Progress
        
        // Perform test
       // Test.startTest();
        //Database.DeleteResult result = Database.delete(acct, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assertEquals( 'Phase 1', 'Phase 1');
    }
    	@isTest static void Test3() 
    {
    
    // Test data setup
        // Create an object
        Case objCaseParent = new Case();
        insert objCaseParent;
 
        Case objCase = new Case();
        objCase.ParentId = objCaseParent.Id;
        insert objCase;

                Case objCaseChild = new Case();
        objCaseChild.ParentId = objCase.Id;
        insert objCaseChild;

        Task objTask = new Task();
        objTask.WhatId = objCase.id;
        
         Test.startTest();


        objCase.Status = 'Closed';
		update objCase;
        
        
        //Invoicing In Progress
        
        // Perform test
       // Test.startTest();
        //Database.DeleteResult result = Database.delete(acct, false);
        Test.stopTest();
        // Verify 
        // In this case the deletion should have been stopped by the trigger,
        // so verify that we got back an error.
        System.assertEquals( 'Phase 1', 'Phase 1');
    }

}