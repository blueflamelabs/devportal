({
	    GetUserInfo: function(component) 
    {
        var action = component.get("c.fetchUser");
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
               // set current user information on userInfo attribute
                component.set("v.userInfo", storeResponse);
                var varUser = component.get("v.userInfo");
                //var varFullName = storeResponse.FirstName + ' ' + storeResponse.LastName; 	
                
                var varcontactid = varUser.ContactId;
                //var varFullName = varUser.Name; 	
               // component.set("v.objPersonRequestingAction",varFullName );
               // 
               // 
                component.set("v.recordId",varcontactid );
               // component.set("v.ContactId",varcontactid );
                
                        // Find the component whose aura:id is "flowData"
        var flow = component.find("flowData");

                 var inputVariables = [
         { name : "varContactId", type : "String", value: component.get("v.recordId")  }

       ];
       //          var inputVariables = [
       //  { name : "varContactId", type : "String", value: component.get("v.ContactId")  }

      // ];
        //         var inputVariables = [
       //  { name : "varContactId", type : "String", value: '0035C00000HVhDAQA1'  }

      // ];

        
        
         flow.startFlow("ServiceRequest", inputVariables);
                
               
            }
        });
        $A.enqueueAction(action);
	},
 GetUserAccount: function(component) 
    {
        var action = component.get("c.fetchUserAccount");
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue().Name;
               // set current user information on userInfo attribute
               // component.set("v.objInstitutionName", storeResponse);
            }
        });
        $A.enqueueAction(action);
	},
    
})