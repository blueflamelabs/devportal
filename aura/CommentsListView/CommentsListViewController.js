({
    setLodashLoaded: function(component,event,helper)
    {
        component.set("v.initializeDone",true);
    },
    
	doInit : function(component, event, helper) 
    {
        //var varobj = $A.get("e.c:CommunityAttributes");
        //                	var strCaseId = varobj.CaseRecordId;
        //    		var strCaseNumber = varobj.CaseNumber;
		//component.set("v.parentid", strCaseId);
        //component.set("v.CaseNumber", strCaseNumber);
        
        helper.SetIsCommunityUser(component, event);
        helper.setColumnWrapper(component, event);
		helper.doInit(component, event);
	},
    
    sort :  function(component,event,helper)
    {
       // event.stopPropagation();
        
       let sortField = event.currentTarget.getAttribute('data-sortField'),
           sortIndex = event.currentTarget.getAttribute('data-sortIndex'),
           sortProperties = component.get("v.sortProperties");   

        if(sortProperties.sortField == sortField) {
            if(sortProperties.sortOrder == 'asc') {
                sortProperties.sortOrder = 'desc';
                
            } else {
                sortProperties.sortOrder = 'asc';
            }
        } else {
            sortProperties.sortField = sortField;
            sortProperties.sortOrder = 'asc';
        }
        
        component.set("v.sortProperties",sortProperties);
        helper.sortBy(component,event);
    },
    
    openCreateModal: function(component, event, helper) 
    {
       component.set("v.newIsPublic", true);
      // Set isModalOpen attribute to true
      component.set("v.isCreateModalOpen", true);
   },
  
   closeCreateModal: function(component, event, helper) 
    {
      // Set isModalOpen attribute to false  
      component.set("v.isCreateModalOpen", false);
   },
  
   submitCreateDetails: function(component, event, helper) 
    {
      helper.submitCreateDetails(component, event, helper);
        
               
   },
        // 060619-T-00185-Method to redirect to the file's parent detail page .
    navigateToParentRecord :function(component,event,helper){
       var navigationSObject = $A.get("e.force:navigateToSObject");
          console.log('navigate--',navigationSObject);
          navigationSObject.setParams({
           "recordId": component.get("v.parentId")
          });
        navigationSObject.fire();
    }, 


})