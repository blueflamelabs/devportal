({
    handleUploadFinished: function (component, event,helper) {
    
        
        var uploadedFiles = event.getParam("files");
        var ids = [];
        for (var i= 0 ; i < uploadedFiles.length ; i++){
            ids.push(uploadedFiles[i].documentId);
        }
        console.log(ids);
       // component.set("v.fileIds",ids);
        helper.updateCaseFlag(component, event,helper,ids,component.get("v.isCommunityUser"));
        var modalBody;
        var modalFooter;
        // 120619 - T - 00252 - Passed isCommUser to filesList component
        $A.createComponents([
            ["c:filesList", { fileIds : ids, isCommUser : component.get("v.isCommunityUser"),}],
            ["c:modalFooter",{fileCount : ids.length}]
        ],
                            function(components, status) {
                                if (status === "SUCCESS") {
                                    modalBody = components[0];
                                    modalFooter = components[1];
                                    component.find('overlayLib').showCustomModal({
                                        header: "Upload Files",
                                        body: modalBody, 
                                        footer: modalFooter,
                                        showCloseButton: true
                                        
                                    })   
                                }
                            });
    }
})