({
    setLodashLoaded: function(component,event,helper){
        component.set("v.initializeDone",true);
    },
	doInit : function(component, event, helper) {
    helper.setColumnWrapper(component, event);
		helper.doInit(component, event);
    //helper.startTimer(component, event, helper);
	},
     sort :  function(component,event,helper){
       // event.stopPropagation();
        /* if(helper.timer){
            window.clearInterval(helper.timer);
            helper.timer = null;
         }*/
       
       let sortField = event.currentTarget.getAttribute('data-sortField'),
          title = event.currentTarget.getAttribute('title'),
           sortIndex = event.currentTarget.getAttribute('data-sortIndex'),
           sortProperties = component.get("v.sortProperties"); 
           console.log('--sortField--',title); 
           var sortTitle = title.charAt(0).toUpperCase() ;
           var lowrstr =  title.substr(1,title.length) ; 
           var titleVar = sortTitle + lowrstr.toLowerCase();
           console.log('--sortTitle--'+titleVar);

        if(sortProperties.sortField == sortField) {
            if(sortProperties.sortOrder == 'asc') {
                sortProperties.sortOrder = 'desc';
                
            } else {
                sortProperties.sortOrder = 'asc';
            }
        } else {
            sortProperties.sortField = sortField;
            sortProperties.sortOrder = 'desc';
        }
        console.log('--sortProperties--',sortProperties);
        component.set("v.sortProperties",sortProperties);
        component.set("v.headerTitle",titleVar);
        component.set("v.displaytimer",true);
        helper.sortBy(component,event);
        //helper.startTimer(component, event, helper);
    },
    
    // 060619-T-00185-Method to redirect to the file's parent detail page .
    navigateToParentRecord :function(component,event,helper){
       var navigationSObject = $A.get("e.force:navigateToSObject");
          console.log('navigate--',navigationSObject);
          navigationSObject.setParams({
           "recordId": component.get("v.rowsToDisplay[0].parentId")
          });
        navigationSObject.fire();
    }, 
})